#!/usr/bin/env bash
if ! [[ -x "$(command -v virtualenv)" ]]
then
    echo "No virtualenv found in \$PATH"
    exit 1
fi

if [[ -x "$(command -v python3.7)" ]]
then
    echo "Found python3.7. Using python3.7 to setup a virtual environment."
    virtualenv --python python3.7 ../venv
elif [[ -x "$(command -v python3)" ]] && python3 --version | grep -E "^Python 3\.7\.[0-9]{0,3}$"
then
    echo "python3 is pointing to the right version. Using python3 to setup a virtual environment."
    virtualenv --python python3 ../venv
else
    echo "This program requires python3.7 to work."
    exit 1;
fi

../venv/bin/pip install -r requirements.pip
echo "Finished setting up the virtual environment."
