from faker import Faker

from providers.CirrusMedProvider import Provider as CirrusProvider


def ignore_args(func):
    def inner(*args, **kwargs):
        return func()

    return inner


class FakerWrapper:
    def __init__(self, seed: int, locale: str):
        fake = Faker()
        fake.seed_locale(locale, seed)
        fake.add_provider(CirrusProvider)
        self.fake = fake

    def get_provider(self, provider: str):
        provider = self.fake.__getattr__(provider)
        # a hack that makes the provider method ignore all the arguments
        return ignore_args(provider)
