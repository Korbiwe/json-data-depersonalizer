import bcrypt

from faker.providers import BaseProvider

LANGUAGES = ['English', 'Spanish', 'Chinese', 'Russian', 'German', 'Arabic', 'Italian', 'Hindi']


class Provider(BaseProvider):
    def cirrus_phone_or_fax_number(self):
        return self.numerify('###-###-####')

    def cirrus_doctor_rating(self):
        return self.random_int(0, 5)

    def cirrus_hashed_password(self):
        return bcrypt.hashpw(b'qwerty1234', bcrypt.gensalt()).decode('utf-8')

    def cirrus_gender(self):
        return self.random_element(('Male', 'Female'))

    def none(self):
        return None

