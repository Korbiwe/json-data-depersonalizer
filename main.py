import json
import sys

import click

from random import getrandbits

from FakerWrapper import FakerWrapper
from Depersonalizer import Depersonalizer


@click.command()
@click.option(
    '--spec',
    default='spec.json',
    help='Path to the specification json file.'
)
@click.option(
    '--data',
    default='data.json',
    help='Path to file with data to depersonalize.'
)
@click.option(
    '--out',
    default=None,
    help='Path to the output file to be produced. If not specified, output will be printed to stdout.'
)
def main(spec, data, out):
    with open(spec) as spec_file, open(data) as data_file:
        data = json.load(data_file)
        spec = json.load(spec_file)

        fake = FakerWrapper(getrandbits(60), spec['locale'])
        depersonalizer = Depersonalizer(spec['paths'], fake)
        depersonalized_data = depersonalizer.depersonalize(data)

        if out is None:
            sys.stdout.write(json.dumps(depersonalized_data))
        else:
            with open(out, 'w') as out_file:
                json.dump(depersonalized_data, out_file)


if __name__ == '__main__':
    main()
