from FakerWrapper import FakerWrapper


class ListProvider:
    def __init__(self, item_specification: dict, fake: FakerWrapper):
        self.factory = Depersonalizer(item_specification, fake).depersonalize

    def depersonalize(self, value):
        return [self.factory(item) for item in value]


class Depersonalizer:
    def __init__(self, specification: dict, fake: FakerWrapper):

        self.fake = fake
        self.providers = {}
        for key, value in specification.items():
            if isinstance(value, dict):
                self.providers[f'fake_{key}'] = Depersonalizer(value, fake).depersonalize
            elif isinstance(value, list):
                self.providers[f'fake_{key}'] = ListProvider(value[0], fake).depersonalize
            else:
                self.providers[f'fake_{key}'] = fake.get_provider(value)

    def depersonalize(self, data):
        result = {}
        for key, value in data.items():
            try:
                provider = self.providers[f'fake_{key}']
                result[key] = provider(value)
            except KeyError:
                # if we didn't have a provider in our spec the item is supposed to remain untouched
                result[key] = value

        return result

